CREATE SCHEMA IF NOT EXISTS tpch10g;
SET SEARCH_PATH TO tpch10g;

CREATE TABLE tpch10g.nation ( 
  N_NATIONKEY  INT,
  N_NAME       VARCHAR(25),
  N_REGIONKEY  INT,
  N_COMMENT    VARCHAR(152)
);    
                        
CREATE TABLE tpch10g.region  ( 
  R_REGIONKEY  INT,
  R_NAME       VARCHAR(25),
  R_COMMENT    VARCHAR(152)
);
CREATE TABLE tpch10g.part  ( 
  P_PARTKEY     INT,
  P_NAME        VARCHAR(55),
  P_MFGR        VARCHAR(25),
  P_BRAND       VARCHAR(10),
  P_TYPE        VARCHAR(25),
  P_SIZE        INT,
  P_CONTAINER   VARCHAR(10),
  P_RETAILPRICE DECIMAL(15,2),
  P_COMMENT     VARCHAR(23)
);
CREATE TABLE tpch10g.supplier ( 
  S_SUPPKEY     INT,
  S_NAME        VARCHAR(25),
  S_ADDRESS     VARCHAR(40),
  S_NATIONKEY   INT,
  S_PHONE       VARCHAR(15),
  S_ACCTBAL     DECIMAL(15,2),
  S_COMMENT     VARCHAR(101)
);
CREATE TABLE tpch10g.partsupp ( 
  PS_PARTKEY     INT,
  PS_SUPPKEY     INT,
  PS_AVAILQTY    INT,
  PS_SUPPLYCOST  DECIMAL(15,2),
  PS_COMMENT     VARCHAR(199)
);
CREATE TABLE tpch10g.customer ( 
  C_CUSTKEY     INT,
  C_NAME        VARCHAR(25),
  C_ADDRESS     VARCHAR(40),
  C_NATIONKEY   INT,
  C_PHONE       VARCHAR(15),
  C_ACCTBAL     DECIMAL(15,2),
  C_MKTSEGMENT  VARCHAR(10),
  C_COMMENT     VARCHAR(117)
);
CREATE TABLE tpch10g.lineitem (
  L_ORDERKEY       INT,
  L_PARTKEY        INT,
  L_SUPPKEY        INT,
  L_LINENUMBER     INT,
  L_QUANTITY       DECIMAL(15,2),
  L_EXTENDEDPRICE  DECIMAL(15,2),
  L_DISCOUNT       DECIMAL(15,2),
  L_TAX            DECIMAL(15,2),
  L_RETURNFLAG     VARCHAR(1),
  L_LINESTATUS     VARCHAR(1),
  L_SHIPDATE       TIMESTAMP,
  L_COMMITDATE     TIMESTAMP,
  L_RECEIPTDATE    TIMESTAMP,
  L_SHIPINSTRUCT   VARCHAR(25),
  L_SHIPMODE       VARCHAR(10),
  L_COMMENT        VARCHAR(44)
);
CREATE TABLE tpch10g.orders (
  O_ORDERKEY       INT,
  O_CUSTKEY        INT,
  O_ORDERSTATUS    VARCHAR(1),
  O_TOTALPRICE     DECIMAL(15,2), 
  O_ORDERDATE      TIMESTAMP,
  O_ORDERPRIORITY  VARCHAR(15),  
  O_CLERK          VARCHAR(15), 
  O_SHIPPRIORITY   INT,
  O_COMMENT        VARCHAR(79)
);

-- Load data
copy tpch10g.nation
from 's3://my-bucket-redshift/nation/nation.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;

copy tpch10g.region
from 's3://my-bucket-redshift/region/region.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;

copy tpch10g.customer
from 's3://my-bucket-redshift/customer/customer.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;
copy tpch10g.lineitem
from 's3://my-bucket-redshift/lineitem/lineitem.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;
copy tpch10g.orders
from 's3://my-bucket-redshift/orders/orders.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;
copy tpch10g.part
from 's3://my-bucket-redshift/part/part.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;

copy tpch10g.partsupp
from 's3://my-bucket-redshift/partsupp/partsupp.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;

copy tpch10g.supplier
from 's3://my-bucket-redshift/supplier/supplier.tbl.gz'
iam_role 'arn:aws:iam::102446636302:role/myRedshiftRole'
delimiter '|'
region 'us-west-2'
gzip
;

---------------------------

select 'customer', count(*) from tpch10g.customer
union
select 'orders', count(*) from tpch10g.orders
union
select 'lineitem', count(*) from tpch10g.lineitem
union
select 'nation', count(*) from tpch10g.nation
union
select 'part', count(*) from tpch10g.part
union
select 'partsupp', count(*) from tpch10g.partsupp
union
select 'region', count(*) from tpch10g.region
union
select 'supplier', count(*) from tpch10g.supplier;

------Housekeeping
analyze;
vacuum;

-------------------------------------------

select l_returnflag, l_linestatus, sum(l_quantity) as sum_qty,
sum(l_extendedprice) as sum_base_price, sum(l_extendedprice*(1-l_discount)) as sum_disc_price,
sum(l_extendedprice*(1-l_discount)*(1+l_tax)) as sum_charge, avg(l_quantity) as avg_qty,
avg(l_extendedprice) as avg_price, avg(l_discount) as avg_disc,  count(*) as count_order
from tpch10g.lineitem
group by l_returnflag, l_linestatus
order by l_returnflag,l_linestatus;

select
       l_returnflag,
       l_linestatus,
       sum(l_quantity) as sum_qty,
       sum(l_extendedprice) as sum_base_price,
       sum(l_extendedprice * (1-l_discount)) as sum_disc_price,
       sum(l_extendedprice * (1-l_discount) * (1+l_tax)) as sum_charge,
       avg(l_quantity) as avg_qty,
       avg(l_extendedprice) as avg_price,
       avg(l_discount) as avg_disc,
       count(*) as count_order
 from
       lineitem
 where
       l_shipdate <= dateadd(day, -90, to_date('1998-12-01', 'YYYY-MM-DD'))
 group by
       l_returnflag,
       l_linestatus
 order by
       l_returnflag,
       l_linestatus;
       

select
n_name, sum(l_extendedprice * (1-l_discount)) as revenue
from
tpch10g.customer c join
( select n_name, l_extendedprice, l_discount, s_nationkey, o_custkey from tpch10g.orders o
join
( select n_name, l_extendedprice, l_discount, l_orderkey, s_nationkey from
tpch10g.lineitem l join
( select n_name, s_suppkey, s_nationkey from tpch10g.supplier s join
( select n_name, n_nationkey
from tpch10g.nation n join tpch10g.region r
on n.n_regionkey = r.r_regionkey and r.r_name = 'MIDDLE EAST'
) n1 on s.s_nationkey = n1.n_nationkey
) s1 on l.l_suppkey = s1.s_suppkey
) l1 on l1.l_orderkey = o.o_orderkey
) o1
on c.c_nationkey = o1.s_nationkey and c.c_custkey = o1.o_custkey
group by n_name
order by revenue desc;

SELECT	O_YEAR,
	SUM(CASE	WHEN	NATION	= 'JAPAN'
			THEN	VOLUME
			ELSE	0
			END) / SUM(VOLUME)	AS MKT_SHARE
FROM	(	SELECT	
                  	extract(year from o_orderdate) as o_year,
			L_EXTENDEDPRICE * (1-L_DISCOUNT)	AS VOLUME,
			N2.N_NAME				AS NATION
		FROM	tpch10g.PART,
			tpch10g.SUPPLIER,
			tpch10g.LINEITEM,
			tpch10g.ORDERS,
			tpch10g.CUSTOMER,
			tpch10g.NATION N1,
			tpch10g.NATION N2,
			tpch10g.REGION
		WHERE	P_PARTKEY	= L_PARTKEY AND
			S_SUPPKEY	= L_SUPPKEY AND
			L_ORDERKEY	= O_ORDERKEY AND
			O_CUSTKEY	= C_CUSTKEY AND
			C_NATIONKEY	= N1.N_NATIONKEY AND
			N1.N_REGIONKEY	= R_REGIONKEY AND
			R_NAME		= 'ASIA' AND
			S_NATIONKEY	= N2.N_NATIONKEY AND
			O_ORDERDATE	BETWEEN '1995-01-01' AND '1996-12-31' AND
			P_TYPE		= 'MEDIUM ANODIZED COPPER'
	)	AS	ALL_NATIONS
GROUP	BY	O_YEAR
ORDER	BY	O_YEAR;

https://github.com/awslabs/amazon-redshift-utils/blob/master/src/CloudDataWarehouseBenchmark/Cloud-DWB-Derived-from-TPCH/3TB/queries/query_0.sql
